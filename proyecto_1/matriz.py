#!/usr/bin/env python
#! -- coding: utf-8 --

mat_cero = []

b = 21
# seccion dedicada a ingresar listas dentro de una lista y rellenar espacios
for i in range(b):
    # agrega listas que actuan como filas
    mat_cero.append([])

    for j in range(b):

        if i == 0 and j !=b-1  or i == b-1 and j !=b-1:
            mat_cero[i].append(str('-'))

        if j == 0 or j == b-1:
            mat_cero[i].append(str('|'))

        else:
            mat_cero[i].append(str(' '))

for i in range(b):
    for j in range(b):

        print(mat_cero[i][j],end=' ')
    print()
